import static org.junit.Assert.*;

import java.util.EmptyStackException;

import ku.util.Stack;
import ku.util.StackFactory;

import org.junit.Test;

/**
 * A JUnit test for {@link Stack} and {@link StackFactory}. Will test almost
 * every behaviors and characteristics.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.3.31
 */
public class StackTest {

	@Test()
	public void testStackCreation_Type0() {
		StackFactory.setStackType(0);
		Stack stack;
		stack = StackFactory.makeStack(0);
		assertEquals(0, stack.capacity());
		assertTrue(stack.isEmpty());
		stack = StackFactory.makeStack(2);
		assertEquals(2, stack.capacity());
		assertTrue(stack.isEmpty());
		assertFalse(stack.isFull());
	}

	@Test
	public void testStackCreation_Type1() {
		StackFactory.setStackType(1);
		Stack stack;
		stack = StackFactory.makeStack(0);
		assertEquals(0, stack.capacity());
		assertTrue(stack.isEmpty());
		stack = StackFactory.makeStack(2);
		assertEquals(2, stack.capacity());
		assertTrue(stack.isEmpty());
		assertFalse(stack.isFull());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNegativeStackSizeCreation_Type0() {
		StackFactory.setStackType(0);
		Stack stack = StackFactory.makeStack(-3);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNegativeStackSizeCreation_Type1() {
		StackFactory.setStackType(1);
		Stack stack = StackFactory.makeStack(-3);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	public void testCapacity_Type0() {
		StackFactory.setStackType(0);
		Stack stack;
		stack = StackFactory.makeStack(0);
		assertEquals(stack.capacity(), 0);
		stack = StackFactory.makeStack(1);
		assertEquals(stack.capacity(), 1);
		stack = StackFactory.makeStack(2);
		assertEquals(stack.capacity(), 2);
		stack = StackFactory.makeStack(10);
		assertEquals(stack.capacity(), 10);
		stack = StackFactory.makeStack(1000);
		assertEquals(stack.capacity(), 1000);
		stack = StackFactory.makeStack(20000);
		assertEquals(stack.capacity(), 20000);
		for (int i = 0; i < 20000; i++) {
			stack.push(i);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	public void testCapacity_Type1() {
		StackFactory.setStackType(1);
		Stack stack;
		stack = StackFactory.makeStack(0);
		assertEquals(stack.capacity(), 0);
		stack = StackFactory.makeStack(1);
		assertEquals(stack.capacity(), 1);
		stack = StackFactory.makeStack(2);
		assertEquals(stack.capacity(), 2);
		stack = StackFactory.makeStack(10);
		assertEquals(stack.capacity(), 10);
		stack = StackFactory.makeStack(1000);
		assertEquals(stack.capacity(), 1000);
		stack = StackFactory.makeStack(20000);
		assertEquals(stack.capacity(), 20000);
		for (int i = 0; i < 20000; i++) {
			stack.push(i);
		}
	}

	@Test
	public void testIsEmpty_Type0() {
		StackFactory.setStackType(0);
		Stack stack = StackFactory.makeStack(50);
		assertTrue(stack.isEmpty());
		for (int i = 1; i <= 50; i++) {
			stack.push(i);
			assertFalse(stack.isEmpty());
		}
		for (int i = 1; i <= 50; i++) {
			assertFalse(stack.isEmpty());
			stack.pop();
		}
		assertTrue(stack.isEmpty());
	}

	@Test
	public void testIsEmpty_Type1() {
		StackFactory.setStackType(1);
		Stack stack = StackFactory.makeStack(50);
		assertTrue(stack.isEmpty());
		for (int i = 1; i <= 50; i++) {
			stack.push(i);
			assertFalse(stack.isEmpty());
		}
		for (int i = 1; i <= 50; i++) {
			assertFalse(stack.isEmpty());
			stack.pop();
		}
		assertTrue(stack.isEmpty());
	}

	@Test
	public void testIsFull_Type0() {
		StackFactory.setStackType(0);
		Stack stack = StackFactory.makeStack(50);
		for (int i = 1; i <= 50; i++) {
			assertFalse(stack.isFull());
			stack.push(i);
		}
		assertTrue(stack.isFull());
		for (int i = 1; i <= 50; i++) {
			stack.pop();
			assertFalse(stack.isFull());
		}
	}

	@Test
	public void testIsFull_Type1() {
		StackFactory.setStackType(1);
		Stack stack = StackFactory.makeStack(50);
		for (int i = 1; i <= 50; i++) {
			assertFalse(stack.isFull());
			stack.push(i);
		}
		assertTrue(stack.isFull());
		for (int i = 1; i <= 50; i++) {
			stack.pop();
			assertFalse(stack.isFull());
		}
	}

	@Test
	public void testPeekEmptyStack_Type0() {
		StackFactory.setStackType(0);
		Stack stack = StackFactory.makeStack(5);
		assertNull(stack.peek());
	}

	@Test
	public void testPeekEmptyStack_Type1() {
		StackFactory.setStackType(1);
		Stack stack = StackFactory.makeStack(5);
		assertNull(stack.peek());
	}

	@Test(expected = EmptyStackException.class)
	public void testPopEmptyStack_Type0() {
		StackFactory.setStackType(0);
		Stack stack = StackFactory.makeStack(5);
		stack.pop();
	}

	@Test(expected = EmptyStackException.class)
	public void testPopEmptyStack_Type1() {
		StackFactory.setStackType(1);
		Stack stack = StackFactory.makeStack(5);
		stack.pop();
	}

	@Test(expected = IllegalStateException.class)
	public void testPushFullStack_Type0() {
		StackFactory.setStackType(0);
		Stack stack = StackFactory.makeStack(50);
		for (int i = 1; i <= 51; i++) {
			stack.push(i);
		}
	}

	@Test(expected = IllegalStateException.class)
	public void testPushFullStack_Type1() {
		StackFactory.setStackType(1);
		Stack stack = StackFactory.makeStack(50);
		for (int i = 1; i <= 51; i++) {
			stack.push(i);
		}
	}

	@Test(expected = IllegalArgumentException.class)
	public void testPushNull_Type0() {
		StackFactory.setStackType(0);
		Stack stack = StackFactory.makeStack(50);
		stack.push(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testPushNull_Type1() {
		StackFactory.setStackType(1);
		Stack stack = StackFactory.makeStack(50);
		stack.push(null);
	}

	@Test
	public void testCommonStackFunctionsAndVerifyItems_Type0() {
		StackFactory.setStackType(0);
		Stack stack = StackFactory.makeStack(50);
		assertTrue(stack.isEmpty());
		assertFalse(stack.isFull());
		for (int i = 1; i <= 50; i++) {
			assertFalse(stack.isFull());
			stack.push(i);
			assertEquals(i, stack.peek());
			assertFalse(stack.isEmpty());
		}
		assertTrue(stack.isFull());
		for (int i = 50; i >= 1; i--) {
			assertFalse(stack.isEmpty());
			assertEquals(i, stack.pop());
			assertFalse(stack.isFull());
		}
		assertFalse(stack.isFull());
		assertTrue(stack.isEmpty());
	}

	@Test
	public void testCommonStackFunctionsAndVerifyItems_Type1() {
		StackFactory.setStackType(1);
		Stack stack = StackFactory.makeStack(50);
		assertTrue(stack.isEmpty());
		assertFalse(stack.isFull());
		for (int i = 1; i <= 50; i++) {
			assertFalse(stack.isFull());
			stack.push(i);
			assertEquals(i, stack.peek());
			assertFalse(stack.isEmpty());
		}
		assertTrue(stack.isFull());
		for (int i = 50; i >= 1; i--) {
			assertFalse(stack.isEmpty());
			assertEquals(i, stack.pop());
			assertFalse(stack.isFull());
		}
		assertFalse(stack.isFull());
		assertTrue(stack.isEmpty());
	}

	@Test
	public void testSize_Type0() {
		StackFactory.setStackType(0);
		Stack stack = StackFactory.makeStack(100);
		for (int i = 1; i <= 100; i++) {
			assertEquals(i - 1, stack.size());
			stack.push(i);
			assertEquals(i, stack.size());
		}
	}

	@Test
	public void testSize_Type1() {
		StackFactory.setStackType(1);
		Stack stack = StackFactory.makeStack(100);
		for (int i = 1; i <= 100; i++) {
			assertEquals(i - 1, stack.size());
			stack.push(i);
			assertEquals(i, stack.size());
		}
	}

}
